// Условное игровое поле
let gameField = [
  ["x", "o", null],
  ["x", null, "o"],
  ["x", "o", "o"],
];

// Деструктуризация клеток поля ввода, в массивы по выигрышным линиям
let line1 = gameField[0]; // row line
let line2 = gameField[1]; // row line
let line3 = gameField[2]; // row line
let line4 = gameField.map((el) => el[0]); // colum line
let line5 = gameField.map((el) => el[1]); // colum line
let line6 = gameField.map((el) => el[2]); // colum line
let line7 = [gameField[0][0], gameField[1][1], gameField[2][2]]; // dioganal line
let line8 = [gameField[0][2], gameField[1][1], gameField[2][0]]; // dioganal line
// Объединение во вложенный массив
let motion = [line1, line2, line3, line4, line5, line6, line7, line8];

// Функция прохода по линиям
const checkWinner = (motion) => {
  if (motion.some((el) => el.every((item) => item == "x"))) {
    console.log("Крестики победили");
  } else if (motion.some((el) => el.every((item) => item == "o"))) {
    console.log("Нолики победили");
  } else if (!motion.some((el) => el.some((item) => item == null))) {
    console.log("Ничья");
  }
}

checkWinner(motion);
