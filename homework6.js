const field = [
  [null, null, null],
  [null, null, null],
  [null, null, null],
];

class GameField {
  constructor(state) {
    this.state = state;
    this.mode = "Крестики";
    this.isOverGame = false;
    this.winner
  }

  getGameFieldStatus() {
    // Деструктуризация клеток поля ввода, в массивы по выигрышным линиям
    let line1 = this.state[0]; // row line
    let line2 = this.state[1]; // row line
    let line3 = this.state[2]; // row line
    let line4 = this.state.map((el) => el[0]); // colum line
    let line5 = this.state.map((el) => el[1]); // colum line
    let line6 = this.state.map((el) => el[2]); // colum line
    let line7 = [this.state[0][0], this.state[1][1], this.state[2][2]]; // dioganal line
    let line8 = [this.state[0][2], this.state[1][1], this.state[2][0]]; // dioganal line
    // Объединение во вложенный массив
    let xoxo = [line1, line2, line3, line4, line5, line6, line7, line8];
    // Проход по игровой доске, поиск победителя
    if (xoxo.some((el) => el.every((item) => item == "X"))) {
      this.isOverGame = true;
      this.winner = 'Победили Крестики!'
    } else if (xoxo.some((el) => el.every((item) => item == "O"))) {
      this.isOverGame = true;
      this.winner = 'Победили Нолики!'
    } else if (!xoxo.some((el) => el.some((item) => item == null))) {
      this.isOverGame = true;
      this.winner = 'Ничья!'
    }
  }

  setMode() {
    return (this.mode = this.mode === "Крестики" ? "Нолики" : "Крестики");
  }

  setFieldCellValue() {
    let a = prompt(`Сейчас ходят ${this.mode},  Введите координату строки:`, "");
    let b = prompt(`Сейчас ходят ${this.mode},  Введите координату столбца:`, "");
    // Проверка введеных координат 
    if (a !== "" && b !== "" && a != null && b != null && a.length < 2 && a.length < 2 
      && !isNaN(a) && !isNaN(b) && a < 4 && b < 4 && a > 0 && b > 0) {
        //Заполнение игровых клеток в зависимости от хода и проверка свободна ли клетка
      if (this.mode == "Крестики") {
        if (this.state[a - 1][b - 1] == null) {
          this.state[a - 1][b - 1] = "X";
          this.setMode();
        } else {
          alert("Игровое поле занято");
        }
      } else if (this.mode == "Нолики") {
        if (this.state[a - 1][b - 1] == null) {
          this.state[a - 1][b - 1] = "O";
          this.setMode();
        } else {
          alert("Игровое поле занято");
        }
      }

    } else {
      alert("Некорректные данные ввода, допустимые значения для ввода 1, 2, 3");
    }
  }
}

const gameField = new GameField(field);

while (!gameField.isOverGame) {
  gameField.setFieldCellValue();
  gameField.getGameFieldStatus();
}

console.log(gameField.winner, gameField.state)

